import request from '@/router/axios';
import {baseUrl} from '@/config/env';

/**
 * jobGroup: 0
 * jobId: 0
 * logStatus: -1
 * filterTime: 2022-12-19 00:00:00 - 2022-12-19 23:59:59
 * start: 0
 * length: 10
 * @param formData
 * @returns {*}
 */
export const joblogList = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/joblog/pageList',
    method: 'get',
    params: formData
});

/**
 * @param id
 * @returns {*}
 */
export const logKill = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/joblog/logKill',
    method: 'get',
    params: {
        id
    }
});


