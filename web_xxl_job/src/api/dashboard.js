import request from '@/router/axios';
import { baseUrl } from '@/config/env';

export const dashboardInfo = () => request({
    url: baseUrl + '/api/xxl-job-admin/dashboardInfo',
    method: 'get'
});

export const chartInfo = (startDate,endDate) => request({
    url: baseUrl + '/api/xxl-job-admin/chartInfo',
    method: 'get',
    params:{
        startDate,endDate
    }
});