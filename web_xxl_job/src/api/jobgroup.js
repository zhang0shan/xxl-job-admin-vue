import request from '@/router/axios';
import {baseUrl} from '@/config/env';

/**
 * appname
 * title
 * start
 * length
 * @param formData
 * @returns {*}
 */
export const jobgroupList = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobgroup/pageList',
    method: 'get',
    params: formData
});

/**
 * appname
 * title
 * addressType:0/1
 * addressList
 * @param formData
 * @returns {*}
 */
export const jobgroupSave = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobgroup/save',
    method: 'get',
    params: formData
});

/**
 * id
 * appname
 * title
 * addressType:0/1
 * addressList
 * @param formData
 * @returns {*}
 */
export const jobgroupUpdate = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/jobgroup/update',
    method: 'get',
    params: formData
});

/**
 *
 * @param id
 * @returns {*}
 */
export const jobgroupRemove = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/jobgroup/remove',
    method: 'get',
    params: {
        id
    }
});

/**
 * @param id
 * @returns {*}
 */
export const jobgroupLoadById = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/jobgroup/loadById',
    method: 'get',
    params: {
        id
    }
});


