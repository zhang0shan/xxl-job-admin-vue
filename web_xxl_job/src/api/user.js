import request from '@/router/axios';
import { baseUrl } from '@/config/env';
export const loginByUsername = (username, password) => request({
    url: '/api/xxl-job-admin/login',
    method: 'post',
    meta: {
        isToken: false
    },
    params: {
        userName:username,
        password
    }
})

export const logout = () => request({
    url: baseUrl + '/api/xxl-job-admin/logout',
    meta: {
        isToken: false
    },
    method: 'post'
})

export const updatePwd = (password) => request({
    url: baseUrl + '/api/xxl-job-admin/user/updatePwd',
    meta: {
        isToken: false
    },
    method: 'post',
    params:{
        password
    }
})

export const refeshToken = () => request({
    url: baseUrl + '/api/xxl-job-admin/user/refesh',
    method: 'post'
})




/**
 * appname
 * title
 * start
 * length
 * @param formData
 * @returns {*}
 */
export const userList = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/user/pageList',
    method: 'get',
    params: formData
});

/**
 * username:a170660
 * password:A170660
 * role:0
 * permission:8,1
 * @param formData
 * @returns {*}
 */
export const userSave = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/user/add',
    method: 'get',
    params: formData
});

/**
 * id
 * username:a170660
 * password:A170660
 * role:0
 * permission:8,1
 * @param formData
 * @returns {*}
 */
export const userUpdate = (formData) => request({
    url: baseUrl + '/api/xxl-job-admin/user/update',
    method: 'get',
    params: formData
});

/**
 *
 * @param id
 * @returns {*}
 */
export const userRemove = (id) => request({
    url: baseUrl + '/api/xxl-job-admin/user/remove',
    method: 'get',
    params: {
        id
    }
});


























export const getUserInfo = () => request({
    url: baseUrl + '/user/getUserInfo',
    method: 'get'
});

export const getMenu = (type = 0) => request({
    url: baseUrl + '/user/getMenu',
    method: 'get',
    data: {
        type
    }
});

export const getTopMenu = () => request({
    url: baseUrl + '/user/getTopMenu',
    method: 'get'
});

export const sendLogs = (list) => request({
    url: baseUrl + '/user/logout',
    method: 'post',
    data: list
})